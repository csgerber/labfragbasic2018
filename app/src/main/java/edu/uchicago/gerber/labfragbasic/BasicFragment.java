package edu.uchicago.gerber.labfragbasic;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class BasicFragment extends Fragment {


    public BasicFragment() {
        // Required empty public constructor
    }


    public static BasicFragment getInstance(int nColor){

        Bundle bundle = new Bundle();
        bundle.putInt("COLOR", nColor);
        BasicFragment basicFragment = new BasicFragment();
        basicFragment.setArguments(bundle);

        return basicFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_basic, container, false);
        Bundle bundle = getArguments();
        int nColor = bundle.getInt("COLOR");

        view.setBackgroundColor(nColor);
        return view;


    }

}
