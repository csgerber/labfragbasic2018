package edu.uchicago.gerber.labfragbasic;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

import edu.uchicago.gerber.labfragbasic.dummy.DummyContent;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener {


    private Button basicButton;
    private Button webButton;
    private Button listButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttons);

        //Create some Fragments and use Fragment manager to swap them into a container.
        basicButton = (Button) findViewById(R.id.basicButton);
        basicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random rnd = new Random();
                //A R G B
                int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


                swapFragmentIntoContainer(BasicFragment.getInstance(color));



            }
        });

        //Create some Fragments and use Fragment manager to swap them into a container.
        webButton = (Button) findViewById(R.id.webButton);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random rnd = new Random();
                //A R G B
                int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


                swapFragmentIntoContainer(WebFragment.getInstance("http://gerber.cs.uchicago.edu/android/"));



            }
        });

        //Create some Fragments and use Fragment manager to swap them into a container.
        listButton = (Button) findViewById(R.id.listButton);
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Random rnd = new Random();
                //A R G B
               // int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


                swapFragmentIntoContainer(ItemFragment.newInstance(2));



            }
        });








    }

    private void swapFragmentIntoContainer(Fragment fragment) {

        FragmentTransaction t = getSupportFragmentManager()
                .beginTransaction();

        t.replace(R.id.container, fragment);
        t.commit();
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        //do something
        Toast.makeText(this, item.content, Toast.LENGTH_LONG).show();
    }
}
